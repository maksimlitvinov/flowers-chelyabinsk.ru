<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

/**@var modX $modx */
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';

$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

if (!isset($_REQUEST['action']) || empty($_REQUEST['action'])) {
    exit(false);
}

require_once MODX_CORE_PATH . 'components/bonus/model/bonus/bonus.class.php';
$bonus = new Bonus($modx);

if (!method_exists($bonus, $_REQUEST['action'])) {
    exit(false);
}

$result = $bonus->{$_REQUEST['action']}();
echo $result;
exit;