miniShop2.plugin.vid = {
            getFields: function(config) {
                        return {
                                    VID: {xtype: 'minishop2-combo-options', description: '<b></b><br />'+_('ms2_product_VID_help')}
                        }
            }
            ,getColumns: function() {
                        return {
                                    VID: {width:50, sortable:false, editor: {xtype:'minishop2-combo-options', name: 'VID'}}
                        }
            }
};