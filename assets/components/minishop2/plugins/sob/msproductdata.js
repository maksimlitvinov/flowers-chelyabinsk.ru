miniShop2.plugin.sob = {
            getFields: function(config) {
                        return {
                                    SOB: {xtype: 'minishop2-combo-options', description: '<b></b><br />'+_('ms2_product_SOB_help')}
                        }
            }
            ,getColumns: function() {
                        return {
                                    SOB: {width:50, sortable:false, editor: {xtype:'minishop2-combo-options', name: 'SOB'}}
                        }
            }
};