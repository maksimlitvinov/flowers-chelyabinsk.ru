miniShop2.plugin.type = {
    getFields: function(config) {
        return {
            TYPE: {xtype: 'minishop2-combo-options', description: '<b></b><br />'+_('ms2_product_TYPE_help')}
        }
    }
    ,getColumns: function() {
        return {
            TYPE: {width:50, sortable:false, editor: {xtype:'minishop2-combo-options', name: 'TYPE'}}
        }
    }
};