$(document).ready(function(){
    $('.sortAndTitleIndex .filters').on('click', function (e) {
        e.preventDefault();
        $('aside .block-check').addClass('show');
    });

    $('#mse2_filters .close').on('click', function (e) {
        e.preventDefault();
        $('aside .block-check').removeClass('show');
    });

    // Опции цветов поштучно НЕ РАБОТАЕТ НИКАК
    if ($('.f-options').length > 0) {
        var wrap = $('.f-options');
        var price = $('.prise .msop2');
        var options = [];

        var op = wrap.find('input[type="checkbox"]').filter(function() {
            return $(this).prop('checked') === true;
        });
        if (op.length > 0) {
            op.each(function() {
                var optionPrice = $(this).data('price');
                var newOptionPrice = Number(price.text()) + Number(optionPrice);
                $(this).parents('form').removeClass('ms2_form').addClass('ms2_op_form');
                price.text(newOptionPrice);
            });
        }

        wrap.find('input.checkbox').on('change', function (e) {
            if ($(this).prop('checked')) {
                var optionPrice = $(this).data('price');
                var newOptionPrice = Number(price.text()) + Number(optionPrice);
                $(this).parents('form').removeClass('ms2_form').addClass('ms2_op_form');
            } else {
                var optionPrice = $(this).data('price');
                var newOptionPrice = Number(price.text()) - Number(optionPrice);
            }
            price.text(newOptionPrice);
        });

        $(document).on('submit', 'form.ms2_op_form', function(e) {
            e.preventDefault();
            var btn = '*[name="ms2_action"]';
            var action = $(this).find(btn).val();

            var op = wrap.find('input[type="checkbox"]').filter(function() {
                 return $(this).prop('checked') === true;
            });

            op.each(function (index) {
                options[index] = {
                    name: $(this).data('name'),
                    price: $(this).data('price')
                }
            });

            var data = {
                action: action,
                id: $(this).find('input[name="id"]').val(),
                count: $(this).find('input[name="count"]').val(),
                price: $(price).text(),
                options: op
            };

            //$.post($(this).attr('action'), data, 'json');
            $.post('/assets/components/minishop2/action.php', data, 'json');
        });
    }

});