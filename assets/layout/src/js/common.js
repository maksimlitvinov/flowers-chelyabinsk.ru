$(function(){

    var Site = {
        init: function () {
            this.load();
        },
        load: function () {
            $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
        },
    };
    Site.init();
    window.Site = Site;

    var Bonus = {
        sel: {
            input: 'input[name="pay_bonus"]',
            cost: '#ms2_order_cost',
            button: 'button[type="submit"]'
        },
        init: function () {
            this.load();
        },
        load: function () {
            var self = this;

            $(document).on('input', this.sel.input, function (e) {
                self.getBalance($(this).val());
            })
        },
        getCost: function () {
            return  parseInt($(this.sel.cost).text().replace(/[^0-9]/g, ''));
        },
        getBalance: function (val) {
            this.send({
                action: 'getBalance'
            }, val);
        },
        send: function (data, val) {
            $.ajax({
                url: 'assets/components/bonus/action.php',
                dataType: 'json',
                type: 'post',
                data: data,
                success: function (balance, status, obj) {
                    var cost = Bonus.getCost();
                    var cost2 = cost / 2;
                    var maxPay = balance > cost2 ? cost2 : balance
                    var message = '';
                    if (!(val <= balance && val <= cost2)) {
                        message = 'Превышена максимальная сумма оплаты бонусами.';
                        Bonus.notOrder(message, maxPay);
                        return false;
                    }
                    Bonus.readyOrder();
                }
            });
        },
        readyOrder: function () {
            $(Bonus.sel.button).prop('disabled', false);
        },
        notOrder: function (message, maxPay) {
            miniShop2.Message.error(message);
            $(Bonus.sel.button).prop('disabled', true);
            $(Bonus.sel.input).val(maxPay);
            Bonus.readyOrder();
        }
    };
    Bonus.init();
    window.Bonus = Bonus;

    var FastOrder = {
        sel: {
            'btn': '.buyoneclk',
            'win': '#fastorder',
            'wrap' : '.b-b',
            'close': '.--close'
        },
        init: function () {
            this.load();
        },
        load: function () {
            var self = this;
            
            $(document).on('click', this.sel.btn, function (e) {
                e.preventDefault();
                var id = $(this).parents('li.product').find('input[name="id"]').val();
                self.send({
                    action: 'getProduct',
                    product_id: id
                });
            });
            $(document).on('click', this.sel.close, function (e) {
                e.preventDefault();
                self.close();
            });
        },
        open: function () {
            $(FastOrder.sel.win + ',' + FastOrder.sel.wrap).fadeIn(200);
        },
        close: function () {
            $(FastOrder.sel.win + ',' + FastOrder.sel.wrap).fadeOut(200);
        },
        send: function (data) {
            var tosend = data || {};
            $.ajax({
                type: "POST",
                url: '/assets/components/fastorder/action.php',
                data: tosend,
                dataType: 'json',
                context: $(FastOrder.sel.win),
                beforeSend: function (jqXHR, settings){
                    var tpl = '<p class="--waiting">Загрузка</p>';
                    $(this).html(tpl);
                    $(FastOrder.sel.win + ',' + FastOrder.sel.wrap).fadeIn(200);
                },
                success: function (data, status, obj) {
                    if (data.success) {
                        $(this).html(data.data);
                    }
                }
            });
        }
    };
    FastOrder.init();
    window.FastOrder = FastOrder;
});