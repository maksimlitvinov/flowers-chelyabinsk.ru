var gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
		sass           = require('gulp-sass'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		del            = require('del'),
		cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
		notify         = require("gulp-notify");

// Пользовательские скрипты проекта

gulp.task('common-js', function() {
	return gulp.src([
		'src/libs/jquery-1.10.1.min.js',
		'src/libs/jquery-1.8.2.min.js',
		'src/libs/cloud-zoom.1.0.2.js',
		'src/libs/jquery.placeholder.min.js',
		'src/libs/ion.rangeSlider.js',
		'src/libs/jquery.fs.stepper.js',
		'src/libs/jquery.cookie.js',
		'src/libs/owl.carousel.min.js',
		'src/libs/scripts.js',
		'src/libs/bootstrap-rating-input.js',
		'src/libs/less.min.js',
		'src/libs/hiraku.min.js',
		'src/libs/custom.js',
		'src/js/common.js',
		])
	.pipe(concat('common.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('src/js'));
});

gulp.task('js', ['common-js'], function() {
	return gulp.src([
		'src/js/common.js', // Всегда в конце
		])
	//.pipe(uglify()) // Минимизировать весь js (на выбор)
	.pipe(gulp.dest('src/js'));
});

gulp.task('sass', function() {
	return gulp.src('src/scss/**/*.scss')
	.pipe(sass({outputStyle: 'expanded'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('src/css'));
});

gulp.task('build', ['removedist', 'sass', 'js'], function() {

	var buildCss = gulp.src([
		'src/css/main.min.css',
		]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'src/js/common.min.js',
		]).pipe(gulp.dest('dist/js'));

	var buildFonts = gulp.src([
		'src/font/**/*',
		]).pipe(gulp.dest('dist/font'));

});

gulp.task('removedist', function() { return del.sync('dist'); });
gulp.task('clearcache', function () { return cache.clearAll(); });

gulp.task('default', ['build']);
