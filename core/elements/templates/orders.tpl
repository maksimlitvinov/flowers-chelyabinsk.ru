{extends 'file:templates/base/base.tpl'}

{block 'template'}
    <div class="template">

        {'!pdoMenu' | snippet : [
            'parents' => 987,
            'outerClass' => 'template__nav'
        ]}

        <div class="orders">
            {var $orders = '@FILE snippets/getUserOrders.php' | snippet : ['limit' => 10]}

            <table class="orders__table">
                <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Номер</th>
                        <th>Сумма</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $orders.items as $item}
                        <tr>
                            <td>{$item.createdon | date : 'd.m.Y'}</td>
                            <td>#{$item.num}</td>
                            <td>{$item.cost} руб.</td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    </div>
{/block}