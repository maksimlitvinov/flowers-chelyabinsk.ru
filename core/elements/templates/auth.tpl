{extends 'file:templates/base/base.tpl'}

{block 'template'}
    <div class="template auth">
        {'!officeAuth' | snippet : [
            'groups' => 'Users',
            'HybridAuth' => 0,
            'loginResourceId' => '989',
            'logoutResourceId' => ('site_start' | config),
            'tplLogin' => '@FILE chanks/lk/auth.tpl'
        ]}
    </div>
{/block}