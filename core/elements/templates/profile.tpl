{extends 'file:templates/base/base.tpl'}

{block 'template'}
    <div class="template">

        {'!pdoMenu' | snippet : [
            'parents' => 987,
            'outerClass' => 'template__nav'
        ]}

        <div class="profile">
            <div class="profile__wrap">
                {'!OfficeProfile' | snippet : [
                    'profileFields' => 'email,fullname,mobilephone,gender',
                    'requiredFields' => 'email,fullname,mobilephone,gender',
                    'HybridAuth' => 0,
                    'tplProfile' => '@FILE chanks/lk/profile.tpl'
                ]}
            </div>
        </div>
    </div>
{/block}