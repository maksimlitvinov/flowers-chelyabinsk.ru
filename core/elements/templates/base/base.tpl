<!DOCTYPE html>
<html lang="ru">
    {include 'head'}
<body>
    <section>
        {include 'header'}
        {include 'menu'}
        {include 'site.benefits'}
        {include 'crumbs'}

        {block 'template'}{/block}

        {include 'footer'}

    </section>
</body>
</html>