<?php
/**
 * @var modX $modx
 * @var pdoFetch $pdoFetch
 */
$pdoFetch = $modx->getService('pdoFetch');

/**
 * Если пользователь авторизован в админке
 * Мы ничего не делаем и отображаем сообщение
 * О неуспешной авторизации
 */
$strError = 'Авторизация прошла некорректно. Пожалуйста, обратитесь к администратору сайта для решения данной проблемы.';
if($modx->user->hasSessionContext('mgr')) {
    return $pdoFetch->getChunk('@INLINE <p>{$textError}</p>', array('textError' => $strError));
}

$userId = $modx->getOption('userId', $scriptProperties, 0);
$limit = $modx->getOption('limit', $scriptProperties, 10);

$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$offset = $page == 1 ? 0 : $page * $limit;

if (empty($userId)) {
    $userId = $modx->user->get('id');
    if(empty($userId)) {return false;}
}

$q = $modx->newQuery('msOrder', array('user_id' => $userId));
$q->sortby('createdon','DESC');
$q->limit($limit, $offset);
$total = $modx->getCount('msOrder', $q);
$orders = $modx->getCollection('msOrder', $q);

$output = array();
foreach($orders as $key => $order) {
    $output[$key] = $order->toArray();
}

return array(
    'items' => $output,
    'total' => $total
);