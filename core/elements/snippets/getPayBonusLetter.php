<?php
$order_id = $modx->getOption('order_id', $scriptProperties, null);
$balance = $modx->getOption('balance', $scriptProperties, null);

if (empty($order_id)) {return array();}

$order = $modx->getObject('msOrder', $order_id);

$pay_bonus = $order->get('pay_bonus');
$cost = $order->get('cost');

$output = array(
    'payment' => $pay_bonus,
    'cost' => $cost - $pay_bonus
);

if (!empty($balance)) {
    $user = $modx->getObject('modUser', $order->get('user_id'));
    $profile = $user->getOne('Profile');
    $output['balance'] = $profile->get('bonusbalans');
}

return $output;