<form id="office-profile-form" class="profile__form" enctype="multipart/form-data" action="" method="post">
    <div class="profile__form-group">
        <div class="profile__form-row">
            <label>{'office_profile_fullname' | lexicon}:</label>
            <input type="text" name="fullname" value="{$fullname}" placeholder="{'office_profile_fullname' | lexicon}"/>
        </div>
        <div class="profile__form-row">
            <label>{'office_profile_email' | lexicon}:</label>
            <input type="text" name="email" value="{$email}" placeholder="{'office_profile_email' | lexicon}"/>
        </div>
        <div class="profile__form-row">
            <label>{'office_profile_phone' | lexicon}:</label>
            <input type="text" name="mobilephone" placeholder="" value="{$mobilephone}"/>
        </div>
        <div class="profile__form-row">
            <label>{'office_profile_password' | lexicon}:</label>
            <input type="password" name="specifiedpassword" value="" placeholder="********"/>
            <div class="help-block message">{$error_specifiedpassword}</div>
            <div class="form-text text-muted">{'office_profile_specifiedpassword_desc' | lexicon}</div>
            <input type="password" name="confirmpassword" value="" placeholder="********"/>
            <div class="help-block message">{$error_confirmpassword}</div>
            <div class="form-text text-muted">{'office_profile_confirmpassword_desc' | lexicon}</div>
        </div>
        <div class="profile__form-row --nowrap">
            <label>Пол:</label>
            <fieldset class="--inline">
                <span><input type="radio" {$gender == 1 ? 'checked' : ''} name="gender" value="1"> Мужской</span>
                <span><input type="radio" {$gender == 2 ? 'checked' : ''} name="gender" value="2"> Женский</span>
            </fieldset>
        </div>
    </div>
    <div class="profile__form-row --nowrap">
        <a class="btn btn-logout" href="{$_modx->resource.id | url : [] : ['action' => 'auth/logout']}">{'office_profile_logout' | lexicon}</a>
        <button type="submit" class="btn btn-prsave">{'office_profile_save' | lexicon}</button>
    </div>
</form>