<div class="title">
    Быстрый заказ
    <a class="order-link --close" href="javascript:;">Закрыть</a>
</div>
    {'!AjaxForm' | snippet : [
        'form' => 'tpl.fastOrder',
        'hooks' => 'notSpam,FormItSaveForm,email',
        'formFields' => 'name,userPhone,product_id,product_nm,product_pr',
        'fieldNames' => 'name==Имя отправителя,userPhone==Телефон,product_id==ID Товара,product_nm==Товар_нм,product_pr==Товар_рп',
        'formName' => 'Быстрый заказ',
        'emailSubject' => 'Купить в один клик',
        'emailTo' => ('ms2_email_manager' | config),
        'validate' => 'name:required,userPhone:required',
        'validationErrorMessage' => 'В форме содержатся ошибки!',
        'successMessage' => 'Заказ успешно отправлен',
        'emailTpl' => 'sentEmailTpl2',
        'frontend_css' => '',
        'id' => $id,
        'thumb' => (('site_url' | config) ~ $thumb),
        'pagetitle' => $pagetitle,
        'price' => $price
    ]}
<div class="konfid">
    Нажимая на кнопку ОТПРАВИТЬ, я даю согласие на обработку <p><a rel="nofollow" target="_blank" href="politika-konfidenczialnosti/">персональных данных</a></p>
</div>