<?php


class FastOrder
{
    /** @var modX $modx */
    public $modx;
    /** @var pdoFetch $pdoFetch */
    public $pdoFetch;
    /** @var miniShop2 $ms2 */
    public $ms2;

    public function __construct(modX $modx, $config = array())
    {
        $this->modx = $modx;
        $this->pdoFetch = $modx->getService('pdoFetch');
        $this->ms2 = $modx->getService('miniShop2');
    }

    public function getProduct($id = 0)
    {
        $tpl = '@FILE chanks/fastorder.tpl';

        $id = empty($id) ? $_REQUEST['product_id'] : $id;

        $where = array(
            'msProduct.id' => $id
        );
        $leftJoin = array(
            array('class' => 'msProductData', 'alias' => 'Data', 'on' => '`msProduct`.`id`=`Data`.`id`'),
        );
        $select = array(
            'msProduct' => 'id,pagetitle',
            'Data' => 'price,thumb'
        );
        $options = array(
            'class' => 'msProduct',
            'parents' => 0,
            'return' => 'data',
            'where' => $where,
            'leftJoin' => $leftJoin,
            'select' => $select
        );

        $res = $this->pdoFetch->getArray('msProduct', $where, $options);
        $res['price'] = $this->ms2->formatPrice($res['price']);
        return array(
            'success' => true,
            'data' => $this->pdoFetch->getChunk($tpl, $res)
        );
    }
}