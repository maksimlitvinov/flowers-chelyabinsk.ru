<?php

if (!class_exists('msPaymentInterface')) {
    require_once dirname(dirname(dirname(__FILE__))) . '/model/minishop2/mspaymenthandler.class.php';
}

class Bonus extends msPaymentHandler implements msPaymentInterface
{

    public function send(msOrder $order)
    {

        if ($order->get('status') > 1) {
            return $this->error('ms2_err_status_wrong');
        }

        $pay_bonus = $_REQUEST['pay_bonus'];
        if (!empty($pay_bonus)) {
            $user_id = $order->get('user_id');
            $user = $this->modx->getObject('modUser', $user_id);
            $profile = $user->getOne('Profile');

            $bonus_balance = $profile->get('bonusbalans');
            if ($pay_bonus <= $bonus_balance) {
                // Code
                $cost = $order->get('cost');
                if ($pay_bonus <= $cost / 2) {
                    $new_bonus_balance = $bonus_balance - $pay_bonus;
                    $profile->set('bonusbalans', $new_bonus_balance);
                    $profile->save();
                    $order->set('pay_bonus', $pay_bonus);
                    $order->save();
                    $this->receive($order);
                }
            }
        }

        return $this->success('', array('msorder' => $order->get('id')));
    }

    public function receive(msOrder $order)
    {
        $miniShop2 = $this->modx->getService('miniShop2');
        $miniShop2->changeOrderStatus($order->get('id'), 5);
        return true;
    }
}