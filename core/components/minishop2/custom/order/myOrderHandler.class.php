<?php
class myOrderHandler extends msOrderHandler {

	public function getCost($with_cart = true, $only_cost = false) {
		$cart = $this->ms2->cart->status();
		$cost = $cart['total_cost'];
		$sum = $this->modx->getOption('free_shipment', null, 10000, true);
		
		if ($cost >= $sum) {
			if (!$with_cart) {
				$cost = 0;
			}
			return $only_cost
				? $cost
				: $this->success('', array('cost' => $cost));
		}
		else {
			return parent::getCost($with_cart, $only_cost);
		}
	}
}