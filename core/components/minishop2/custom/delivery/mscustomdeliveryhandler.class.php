<?php

class mscustomdeliveryhandler extends msDeliveryHandler {

    public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0) {
        $cart = $this->ms2->cart->status();
        
        /**
         * Получение цен из настроек
         */
        $delivery_time_before_midnight = $this->modx->getOption('delivery_time_before_midnight', $scriptProperties, 100);
        $delivery_time_after_midnight = $this->modx->getOption('delivery_time_after_midnight', $scriptProperties, 200);
        $delivery_free = $this->modx->getOption('delivery_free', $scriptProperties, 5000);
        
        if (count($cart_get) == $counter) {
		    $arr = $order->get();
		    
		    if ($arr['room'] == 'с 21:00 до 0:00') {
		        if ($cart['total_cost'] < $delivery_free) {
		            $cost += $delivery->get('price') + $delivery_time_before_midnight;
		        }
		        else { 
		             $cost += $delivery_time_before_midnight;
		        }
    		    return $cost;
		    }
		    else if ($arr['room'] == 'с 0:00 до 9:00') {
		            if ($cart['total_cost'] < $delivery_free) {
		                $cost += $delivery->get('price') + $delivery_time_after_midnight;
		            }
		            else { 
		                 $cost += $delivery_time_after_midnight;
		            }
    		  	return $cost;
		    }
		}
		
        if ($cart['total_cost'] < $delivery_free) {
    		$add_price = $delivery->get('price');
    		if (preg_match('/%$/', $add_price)) {
    			$add_price = str_replace('%', '', $add_price);
    			$add_price = $cost / 100 * $add_price;
    			
    		}
    		
    		$cost += $add_price;
        }
		return $cost;
	}
}

