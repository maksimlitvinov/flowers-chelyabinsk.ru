<?php

interface msDeliveryInterface {

	/**
	 * Returns an additional cost depending on the method of delivery
	 *
	 * @param msOrderInterface $order
	 * @param msDelivery $delivery
	 * @param float $cost
	 *
	 * @return float|integer
	 */
	public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0);


	/**
	 * Returns failure response
	 *
	 * @param string $message
	 * @param array $data
	 * @param array $placeholders
	 *
	 * @return array|string
	 */
	public function error($message = '', $data = array(), $placeholders = array());


	/**
	 * Returns success response
	 *
	 * @param string $message
	 * @param array $data
	 * @param array $placeholders
	 *
	 * @return array|string
	 */
	public function success($message = '', $data = array(), $placeholders = array());
}


class msDeliveryHandler implements msDeliveryInterface {
	/** @var modX $modx */
	public $modx;
	/** @var miniShop2 $ms2 */
	public $ms2;


	/**
	 * @param xPDOObject $object
	 * @param array $config
	 */
	function __construct(xPDOObject $object, $config = array()) {
		$this->modx = & $object->xpdo;
		$this->ms2 = & $object->xpdo->getService('minishop2');
	}


	/** @inheritdoc} */
	public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0) {
		$cart = $this->ms2->cart->status();
		$weight_price = $delivery->get('weight_price');
		//$distance_price = $delivery->get('distance_price');
		
		/**
         * Получение цен из настроек
         */
        $delivery_time_before_midnight = $this->modx->getOption('delivery_time_before_midnight', $scriptProperties, 100);
        $delivery_time_after_midnight = $this->modx->getOption('delivery_time_after_midnight', $scriptProperties, 200);
		
		$cart_weight = $cart['total_weight'];
		$cost += $weight_price * $cart_weight;
		
		$arr = array(
            128,
            55,
        );
        $counter = 0;
		$cart_get = $this->ms2->cart->get();
		foreach($cart_get as $item) {
            foreach($this->modx->getParentIds($item['id']) as $item_id) {
                if(in_array($item_id, $arr)) {
                    $counter++;
                }
            }
		}
		
		if(count($cart_get) == $counter) {
		    $arr = $order->get();
		    
		    if($arr['room'] == 'с 21:00 до 0:00' || $arr['room'] == 'с 0:00 до 9:00') {
		        if($delivery->get('price') == 0) {
    		        $delivery->set('price', 0);
    		    }
		    } else {
		        if($delivery->get('price') == 0) {
    		        //$delivery->set('price',  $delivery_time_after_midnight);
    		        $delivery->set('price',  0);
    		    }
		    }
		}

		$add_price = $delivery->get('price');
		if (preg_match('/%$/', $add_price)) {
			$add_price = str_replace('%', '', $add_price);
			$add_price = $cost / 100 * $add_price;
		}
		
		
		
		
		
// CUSTOM CODE
		if(isset($_COOKIE['delivery_night'])) {
		    $cookie = $_COOKIE['delivery_night'];
		    $cookie = (int)$cookie;
		    
    		if($cookie === 1) 
                $add_price +=  $delivery_time_before_midnight;
    		else if($cookie === 2) 
                $add_price +=  $delivery_time_after_midnight;
		}
// END CUSTOM CODE
            
		$cost += $add_price;
		return $cost;
	}


	/** @inheritdoc} */
	public function error($message = '', $data = array(), $placeholders = array()) {
		if (!is_object($this->ms2)) {
			$this->ms2 = $this->modx->getService('minishop2');
		}
		return $this->ms2->error($message, $data, $placeholders);
	}


	/** @inheritdoc} */
	public function success($message = '', $data = array(), $placeholders = array()) {
		if (!is_object($this->ms2)) {
			$this->ms2 = $this->modx->getService('minishop2');
		}
		return $this->ms2->success($message, $data, $placeholders);
	}
}