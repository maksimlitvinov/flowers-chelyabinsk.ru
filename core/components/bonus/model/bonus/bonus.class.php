<?php

class Bonus
{
    /** @var modX $modx */
    public $modx;
    /** @var miniShop2 $ms2 */
    public $ms2;
    /** @var modUserProfile $profile */
    public $profile;

    public function __construct(modX &$modx,array $config = array())
    {
        $this->modx = $modx;
        $this->ms2 = $modx->getService('miniShop2');
        $this->ms2->initialize();
        $this->profile = $this->modx->user->getOne('Profile');
    }

    public function getBalance()
    {
        return $this->profile->get('bonusbalans');
    }
}